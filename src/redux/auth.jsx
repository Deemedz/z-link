import { createSlice } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";

// Initial state
const initialState = {
  refreshToken: "",
  accessToken: "",
  user: {
    userId: null,
    firstName: "",
    lastName: "",
    emailAddress: "",
    phoneNumber: "",
    bio: "",
    profilePic: null,
    socials: [],
  },
  gigs: [],
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    // This method updates the session state
    setValues(state, action) {
      return {
        ...state,
        refreshToken: action.payload.refreshToken,
        accessToken: action.payload.accessToken,
        user: {
          userId: action.payload.user.user_id,
          firstName: action.payload.user.first_name,
          lastName: action.payload.user.last_name,
          emailAddress: action.payload.user.email,
          phoneNumber: action.payload.user.phone_number,
          bio: action.payload.user.bio,
          profilePic: action.payload.user.profile_picture,
          socials: action.payload.user.socials,
        },
      };
    },
    // This method clears the session to log out the user
    resetGlobalState() {
      storage.removeItem("persist:root");
    },
    // This updates the user after retrieval
    setUpdatedValues(state, action) {
      return {
        ...state,
        user: {
          // spreads the user object so only specific fields are updated
          ...state.user,
          firstName: action.payload.first_name,
          lastName: action.payload.last_name,
          emailAddress: action.payload.email,
          phoneNumber: action.payload.phone_number,
          bio: action.payload.bio,
        },
      };
    },
    // saves the state of the active gigs list
    setGigValues(state, action) {
      return {
        ...state,
        gigs: action.payload,
      };
    },
  },
});

export const { setValues, resetGlobalState, setUpdatedValues, setGigValues } =
  authSlice.actions;

export default authSlice.reducer;
