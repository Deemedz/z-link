import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	openDashBoard: false,
};

const dashBoardSlice = createSlice({
	name: "dashBoard",
	initialState,
	reducers: {
		handleOpenDashBoard(state) {
			return {
				...state,
				openDashBoard: !state.openDashBoard,
			};
		},
	},
});

export const { handleOpenDashBoard } = dashBoardSlice.actions;

export default dashBoardSlice.reducer;
