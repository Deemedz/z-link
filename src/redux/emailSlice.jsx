import { createSlice } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";

// Initial state
const initialState = {
  user: {
    // firstName: "",
    // lastName: "",
    // emailAddress: "",
    // phoneNumber: "",
    // bio: "",
    // profilePic: null,
    // socials: [],
  },
  gig: {},
  url: null,
};

const emailContentSlice = createSlice({
  name: "emailSlice",
  initialState,
  reducers: {
    setContentValues(state, action){
        return{
            ...state,
            user: action.payload.user,
            gig: action.payload.gig,
            url: action.payload.url
        }
    },
  },
});

export const {setContentValues} = emailContentSlice.actions;

export default emailContentSlice.reducer;
