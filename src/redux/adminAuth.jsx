import { createSlice } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";

// Initial state
const initialState = {
  refreshToken: "",
  accessToken: "",
  isLoggedIn: false,
  isAdmin: false,
  users: [],
  gigs: [],
  gig:{}
};
// creates the admin auth slice
const adminAuthSlice = createSlice({
  name: "adminAuth",
  initialState,
  reducers: {
    setAdminValues(state, action) {
      return {
        ...state,
        refreshToken: action.payload.refreshToken,
        accessToken: action.payload.accessToken,
        isLoggedIn: true,
        isAdmin: true,
      };
    },
    setUserList(state, action) {
      return {
        ...state,
        users: action.payload,
      };
    },
    setGigList(state, action) {
      return {
        ...state,
        gigs: action.payload,
      };
    },
    setSelectedGig(state, action){
      return{
        ...state,
        gig: action.payload.gig,
      }
    },
  },
});

export let { setAdminValues, setUserList, setGigList, setSelectedGig } = adminAuthSlice.actions;

export default adminAuthSlice.reducer;
