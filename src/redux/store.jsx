import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import authReducer from "./auth";
import adminAuthReducer from "./adminAuth"
import emailReducer from "./emailSlice"
import dashboardReducer from "./dashboard";

const rootReducer = combineReducers({
  adminAuth: adminAuthReducer,
  auth: authReducer,
  emailSlice: emailReducer,
  dashBoard: dashboardReducer,
});

const persistConfig = {
	key: "root",
	storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default () => {
	let store = configureStore({
		reducer: persistedReducer,
	});
	let persistor = persistStore(store);
	return { store, persistor };
};
