import React from "react";
import Footer from "../../components/Footer/Footer";
import AdminDashBoard from "../../components/Admin/AdminDashBoard";
import AdminNavBar from "../../components/Navbar/AdminNavBar";
import ModalComp from "../../components/Modal/ModalComp";

const AdminDashBoardPage = () => {
  return (
    <div className="">
      <AdminNavBar />
      <AdminDashBoard />
      <Footer />
    </div>
  );
};

export default AdminDashBoardPage;
