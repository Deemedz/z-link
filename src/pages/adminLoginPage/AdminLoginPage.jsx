import React from "react";
import NavBar from "../../components/Navbar/NavBar";
import Footer from "../../components/Footer/Footer";
import AdminLogin from "../../components/Admin/AdminLogin";

const AdminLoginPage = () => {
	return (
		<div>
			<NavBar />
			<AdminLogin />
			<Footer />
		</div>
	);
};

export default AdminLoginPage;
