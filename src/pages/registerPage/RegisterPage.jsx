import React from "react";
import NavBar from "../../components/Navbar/NavBar";
import Footer from "../../components/Footer/Footer";
import Register from "../../components/Register/Register";

const RegisterPage = () => {
	return (
		<div>
			<NavBar />
			<Register />
			<Footer />
		</div>
	);
};

export default RegisterPage;
