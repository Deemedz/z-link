import React from "react";
import NavBar from "../../components/Navbar/NavBar";
import Profile from "../../components/Profile/Profile";
import Footer from "../../components/Footer/Footer";
import { useSelector } from "react-redux";

const ProfilePage = () => {
	const auth = useSelector((state) => state.auth);
	return auth.refreshToken != null ? (
		<div>
			<NavBar />
			<Profile />
			<Footer />
		</div>
	) : (
		<div>Error</div>
	);
};

export default ProfilePage;
