import React from "react";
import NavBar from "../../components/Navbar/NavBar";
import Home from "../../components/Home/Home";
import AboutUs from "../../components/AboutUs/AboutUs";
import Footer from "../../components/Footer/Footer";
import Sponsors from "../../components/Sponsors/Sponsors";

const HomePage = () => {
  return (
    <div>
      <NavBar />
      <Home />
      <AboutUs />
      <Sponsors />
      <Footer />
    </div>
  );
};

export default HomePage;
