import React from "react";
import NavBar from "../../components/Navbar/NavBar";
import Footer from "../../components/Footer/Footer";
import Login from "../../components/Login/Login";

const LoginPage = () => {
	return (
		<div>
			<NavBar />
			<Login />
			<Footer />
		</div>
	);
};

export default LoginPage;
