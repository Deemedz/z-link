import React from "react";
import NavBar from "../../components/Navbar/NavBar";
import GigBoard from "../../components/GigBoard/GigBoard";
import Footer from "../../components/Footer/Footer";
import { useSelector } from "react-redux";
import DashboardMenu from "../../components/Admin/DashBoard/DashboardMenu";
import AdminNavBar from "../../components/Navbar/AdminNavBar";

const GigBoardPage = () => {
	const auth = useSelector((state) => state.auth);
	return auth.refreshToken != null ? (
    <div>
      <NavBar />
      <GigBoard />
      <Footer />
    </div>
  ) : (
    <div>Error</div>
  );
};

export default GigBoardPage;
