
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Provider } from "react-redux";
import reduxStore from "./redux/store.jsx";
import { PersistGate } from "redux-persist/integration/react";
import HomePage from "./pages/homePage/HomePage.jsx";
import PageNotFound from "./pages/pageNotFound/PageNotFound.jsx";
import LoginPage from "./pages/loginPage/LoginPage.jsx";
import RegisterPage from "./pages/registerPage/RegisterPage.jsx";
import ProfilePage from "./pages/profilePage/ProfilePage.jsx";
import AdminLoginPage from "./pages/adminLoginPage/AdminLoginPage.jsx";
import AdminDashBoardPage from "./pages/adminDashBoardPage/AdminDashBoardPage.jsx";
import GigBoardPage from "./pages/gigBoard/GigBoardPage.jsx";
import AdminGigDashboard from "./components/Admin/AdminGigDashboard.jsx";
import ProtectedRoute from "./ProtectedRoute.jsx";

const App = () => {
  let store = reduxStore().store;
  let persistor = reduxStore().persistor;

  let adminProtectedRoute = <ProtectedRoute isAdmin={true} />;
  let userProtectedRoute = <ProtectedRoute isUser={true} />;
  return (

    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <div>
            <Routes>
              <Route exact path="/" element={<HomePage />} />
              <Route path="/login" element={<LoginPage />} />
              <Route path="/register" element={<RegisterPage />} />
              <Route path="/login/admin" element={<AdminLoginPage />} />
              <Route path="/" element={adminProtectedRoute}>
                <Route path="/dashboard" element={<AdminDashBoardPage />} />
                <Route path="/admin/gigs" element={<AdminGigDashboard />} />
              </Route>
              <Route path="/" element={userProtectedRoute}>
                <Route path="/profile" element={<ProfilePage />} />
                <Route path="/gig-board" element={<GigBoardPage />} />
              </Route>
              <Route path="/error" element={<PageNotFound />} />
            </Routes>
          </div>
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
};

export default App;
