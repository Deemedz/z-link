import React from "react";

const TextArea = (props) => {
	return (
		<div className="text-black">
			<textarea
				onChange={props.onChange}
				id={props.id}
				className={`my-4 sm:my-2 duration-300 shadow-xl text-sm rounded-lg block p-2.5 w-72 ${props.classNames}`}
				placeholder={props.name}
				required={props.required ? true : false}
			/>
			{/*
  <p class={props.error ? "mt-2 text-sm text-red-600 dark:text-red-500" : "hidden"}>Email Address or Password is incorrect</p>*/}
		</div>
	);
};

export default TextArea;
