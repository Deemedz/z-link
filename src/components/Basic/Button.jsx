import React from "react";

const Button = (props) => {
	let primaryStyle = "text-white border-solid shadow-xl sm:my-2 hover:shadow-2xl bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-xl text-sm w-full w-auto px-5 py-2.5 text-center my-4 duration-300"
	let secondaryStyle = "text-black border-solid shadow-xl sm:my-2 hover:shadow-2xl bg-white hover:bg-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-600 font-medium rounded-xl text-sm w-full w-auto px-5 py-2.5 text-center my-4 duration-300"
	return (
		<button
			type={props.type}
			onClick={props.onClick}
			class={`${props.classname} ${
				props.primary
					? primaryStyle
					: secondaryStyle
			}`}
		>
			{props.name}
		</button>
	);
};

export default Button;
