import React from "react";

const TextArea = (props) => {
  return (
    <div
      className={`flex flex-row border-[1px] border-blue-700 rounded-lg w-full bg-[#E8F0FE] p-[8px] ${props.containerClassname}`}
    >
      <div
        className={`w-[24px] h-[24px] text-[#82858f] items-start ${props.iconClassname}`}
      >
        {props.textareaIcon}
      </div>
      <textarea
        className={`w-full text-[13px] bg-[#E8F0FE] pl-[8px] font-[500] border-none focus:outline-none ${props.textareaClassname}`}
        id={props.id}
        type={props.type}
        onChange={props.onChange}
        placeholder={props.placeholder}
        value={props.value ? props.value : null}
        maxLength={props.length}
      />
    </div>
  );
};

export default TextArea;
