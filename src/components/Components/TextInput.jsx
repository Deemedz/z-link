import React from "react";

const TextInput = (props) => {
  return (
    <div
      className={`flex flex-row items-center bg-[#E8F0FE] h-full rounded-lg w-full px-[8px] ${props.containerClassname}`}
    >
      <div
        className={`w-[24px] h-[24px] text-[#82858f] ${props.iconClassname}`}
      >
        {props.inputIcon}
      </div>
      <input
        className={`text-black p-[10px] w-full border-none focus:outline-none focus:text-[13px] text-[13px] font-[500] bg-[#E8F0FE] ${props.inputClassname}`}
        id={props.id}
        type={props.type}
        onChange={props.onChange}
        placeholder={props.placeholder}
        step={props?.step}
        value={props.value ? props.value : null}
      />
      <div
        className={`w-[24px] h-[24px] text-[#82858f] hover:cursor-pointer ${props.iconClassname}`}
        onClick={props.iconClick}
      >
        {props.secondaryIcon}
      </div>
    </div>
  );
};

export default TextInput;
