import React from "react";
import Logo from "../../assets/Z-link-Logo-Only.png";
import "./Home.css";

const Home = () => {
  return (
    <div className="w-full h-full ">
      <div
        id="home-bg"
        name="home"
        class="w-auto h-[1024px] flex justify-around items-center mx-auto px-8 bg-auto bg-no-repeat bg-center"
      >
        <div className="w-auto h-[500px]">
          <div className="flex-col items-center justify-around">
            <p className="font-bold xl:text-8xl md:text-6xl text-3xl text-white py-4 px-6">
              The Link to the Ecosystem of Caribbean Creators
            </p>

            <div className="md:flex flex-row w-auto justify-center mx-auto px-8 ">
              <p className="font-medium xl:text-3xl md:text-2xl text-lg text-white py-4">
                The Link to the Ecosystem Caribbean Creators
              </p>
              <img
                alt="rocket"
                src={Logo}
                className="xl:w-[500px] xl:h-[500px] md:w-[400px] md:h-[400px] w-[300px] h-[300px] m-10 animate-rocketBounce lg:-translate-y-48 md:-translate-y-32 -translate-y-16"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
