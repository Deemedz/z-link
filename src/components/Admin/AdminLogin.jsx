// TODO!!! Create an admin auth and authslice
import React, { useState } from "react";
import Button from "../Basic/Button";
import "./AdminLogin.css";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import AdminRequests from "../../utils/adminRequests";
import { setAdminValues } from "../../redux/adminAuth";
import TextInput from "../Components/TextInput";
import { ReactComponent as LockIcon } from "../../assets/icons/lock-closed-outline.svg";
import { ReactComponent as VisibleIcon } from "../../assets/icons/eye-outline.svg";
import { ReactComponent as InvisibleIcon } from "../../assets/icons/eye-off-outline.svg";
import { ReactComponent as UserIcon } from "../../assets/icons/person-circle-outline.svg";

const AdminLogin = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [idText, setIDText] = useState("");
  const [passwordText, setPasswordText] = useState("");
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [error, setError] = useState(false);

  const onChangeID = (e) => {
    setIDText(e.target.value);
  };

  const onChangePassword = (e) => {
    setPasswordText(e.target.value);
  };

  let loginAdminThunk = async (payload) => {
    AdminRequests.login(payload).then((data) => {
      if (data.error) {
        navigate("/error");
      } else {
        dispatch(setAdminValues(data["data"]));
        navigate("/dashboard");
      }
    });
  };

  let login = async () => {
    var payload = {
      id: idText,
      password: passwordText,
    };
    await loginAdminThunk(payload);
  };

  // toggle for password visibility
  const handlePasswordIcon = () => {
    setPasswordVisible(!passwordVisible);
  };

  return (
    <div className="w-full h-full">
      <div
        id="login-bg"
        class="w-auto h-screen flex justify-around items-center mx-auto py-24 px-8 bg-auto bg-no-repeat"
      >
        <div className="w-auto h-[500px] flex items-center">
          <div className="flex-col items-center justify-around space-y-[10px]">
            <h1 className="text-3xl font-bold text-center text-white mb-4">
              Admin Login
            </h1>
            <TextInput
              id="id"
              type="number"
              name="ID"
              onChange={onChangeID}
              placeholder="00000000"
              value={idText}
              inputIcon={<UserIcon />}
            />
            <TextInput
              id="password"
              type={passwordVisible ? "text" : "password"}
              onChange={onChangePassword}
              inputIcon={<LockIcon />}
              placeholder="***********"
              value={passwordText}
              iconClick={handlePasswordIcon}
              secondaryIcon={
                passwordVisible ? <VisibleIcon /> : <InvisibleIcon />
              }
            />
            <Button
              type="submit"
              name="Log In"
              onClick={() => {
                idText !== "" && passwordText !== "" ? login() : setError(true);
              }}
              primary
            />
            {error ? (
              <p className="text-xs text-red-600 underline">
                Error logging in, please try again.
              </p>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdminLogin;
