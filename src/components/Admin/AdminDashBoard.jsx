// TODO!!! Update the styling of the table
import React, { useEffect } from "react";
import "./AdminLogin.css";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Table from "../TableComp/Table";
import AdminRequests from "../../utils/adminRequests";
import { setUserList } from "../../redux/adminAuth";

const AdminDashBoard = () => {
  // VARIABLES
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let admin = useSelector((state) => state.adminAuth);

  // Method to get all users from the database
  let getUsers = async () => {
    var payload = {
      accessToken: admin.accessToken,
    };
    AdminRequests.getAllUsers(payload).then((data) => {
      if (data.error) {
        navigate("/error");
      } else {
        dispatch(setUserList(data["data"]));
      }
    });
  };

  useEffect(() => {
    getUsers();
  }, []);

  return (
    <div
      id="login-bg"
      class="px-[20px] md:w-screen min-h-screen py-[40px] flex justify-center items-center flex-col"
    >
      <div className="md:flex md:justify-center w-full h-max   ">
        <Table
          headers={admin?.users[0]}
          rows={admin?.users}
          title="User Table"
          icon={null}
        ></Table>
      </div>
    </div>
  );
};

export default AdminDashBoard;
