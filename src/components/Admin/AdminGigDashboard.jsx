// TODO!!! Update the styling of the table
import React, { useState, useEffect } from "react";
import "./AdminLogin.css";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Table from "../TableComp/Table";
import AdminRequests from "../../utils/adminRequests";
import { setGigList } from "../../redux/adminAuth";
import AdminNavBar from "../Navbar/AdminNavBar";
import ModalComp from "../Modal/ModalComp";
import GigForm from "../Forms/GigForm";
import { ReactComponent as AddIcon } from "../../assets/svg/add-outline.svg";

const AdminGigDashboard = () => {
  // VARIABLES
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let admin = useSelector((state) => state.adminAuth);

  // USESTATE
  let [isModalVisible, setIsModalVisible] = useState(false);
  let [isEditVisible, setIsEditVisible] = useState(false);

  // This method updates the state for the modal's visibility
  let updateModalVisibility = () => {
    setIsModalVisible(!isModalVisible);
  };
  let updateEditModalVisibility = () => {
    setIsEditVisible(!isEditVisible);
  };

  // This methods gets all the gigs available
  let getGigs = async () => {
    var payload = {
      accessToken: admin.accessToken,
    };
    AdminRequests.getAllGigs(payload).then((data) => {
      if (data.error) {
        navigate("/error");
      } else {
        dispatch(setGigList(data["data"]));
      }
    });
  };

  // places the component in a variable to pass
  let GigFormComponent = (
    <GigForm onConfirmClick={getGigs} onCancelClick={updateModalVisibility} />
  );

  // USEEFFECT
  useEffect(() => {
    getGigs();
  }, []);

  return (
    <>
      <AdminNavBar />
      <div
        id="login-bg"
        class="w-screen min-h-screen py-[40px] justify-center items-center flex flex-col px-[20px]"
      >
        <div className="w-full overflow-x-scroll flex justify-center">
          <Table
            headers={admin?.gigs[0]}
            rows={admin?.gigs}
            title="Gig Table"
            icon={<AddIcon onClick={updateModalVisibility} />}
            onEdit={updateEditModalVisibility}
            onClickGetData={getGigs}
          />
        </div>

        {/* This instance of the modal is to create a new gig  */}
        {isModalVisible ? (
          <ModalComp
            title="Create Gig"
            component={GigFormComponent}
            secondClassName="relative m-auto min-w-[30%] mt-[12%]"
            visibility={() => {
              updateModalVisibility();
            }}
          />
        ) : null}
      </div>
    </>
  );
};

export default AdminGigDashboard;
