import React from "react";
import DashboardMenuItem from "./DashboardMenuItem";
import { FaBars } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { handleOpenDashBoard } from "../../../redux/dashboard";

const DashboardMenu = (props) => {
	const dispatch = useDispatch();
	const dashboardMenuTitle = props.title;
	const menuItems = ["Users", "Gigs", "Logout"];

	var dashBoard = useSelector((state) => state.dashBoard);

	const handleDashBoard = () => {
		dispatch(handleOpenDashBoard());
	};

	return (
    <div>
      <div
        className={
          dashBoard.openDashBoard
            ? "fixed h-full w-[300px] duration-300 lg:translate-x-0 flex-col justify-between items-start px-4 pt-5 bg-black bg-opacity-30 text-white shadow-black shadow-inner"
            : "fixed h-[80px] w-[300px] flex-col justify-between items-start px-4 pt-5 bg-transparent duration-500 text-white"
        }
      >
        <div
          onClick={handleDashBoard}
          className="text-3xl font-bold underline py-2"
        >
          <FaBars />
          {dashBoard.openDashBoard ? dashboardMenuTitle : null}
        </div>

        <div
          className={
            dashBoard.openDashBoard
              ? "text-xl font-medium py-2 duration-300"
              : "hidden duration-300"
          }
        >
          <ul>
            <DashboardMenuItem
              itemName={menuItems[0]}
              selected={true}
              handleSelect={() => {}}
              pageRedirect='/dashboard'
            />
          </ul>
          <ul>
            <DashboardMenuItem
              itemName={menuItems[1]}
              selected={false}
              handleSelect={() => {}}
              pageRedirect="/gig-board"
            />
          </ul>
          <ul>
            <DashboardMenuItem
              itemName={menuItems[2]}
              selected={false}
              handleSelect={() => {}}
              pageRedirect=''
            />
          </ul>
        </div>
      </div>
    </div>
  );
};

export default DashboardMenu;
