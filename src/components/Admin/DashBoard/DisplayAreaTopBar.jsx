import React, { useState, useEffect } from "react";
import { FaBars, FaTimes } from "react-icons/fa";

const DisplayAreaTopBar = (props) => {
	const handleClick = () => props.openDashBoard;

	return (
		<div className="fixed w-full h-[80px] flex lg:pr-72 lg:hidden justify-between items-center px-4 bg-slate-300 text-black">
			{/* Nav Bar Title Text
			<div>DashBoard</div>
      */}
			{/*
      Nav Bar Title Logo
      <div>
        <img src={Logo} alt="Logo" style={{ width: "75px" }} />
      </div>
      */}

			{/* Hamburger Menu */}
			<div onClick={handleClick} className=" z-30 flex flex-row">
				<FaBars />
				<div className="translate-x-5 -translate-y-1 text-lg">DashBoard</div>
			</div>
		</div>
	);
};

export default DisplayAreaTopBar;
