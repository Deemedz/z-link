import React from "react";
import { useNavigate } from "react-router-dom";

const DashboardMenuItem = (props) => {
	const itemName = props.itemName;
	const selected = props.selected;
	const pageRedirect = props.pageRedirect;
	const navigate = useNavigate();

	return (
    <button onClick={() => {navigate(pageRedirect)}}>
      <li
        className={
          selected
            ? "py-4 px-0 transform duration-300 text-sky-300 scale-110 translate-x-3"
            : "py-4 px-0 transform duration-300 hover:text-sky-300 hover:scale-110 hover:translate-x-3"
        }
      >
        {itemName}
      </li>
    </button>
  );
};

export default DashboardMenuItem;
