import React from "react";
import { useDispatch, useSelector } from "react-redux";

const DashboardDisplayArea = (props) => {
	var dashBoard = useSelector((state) => state.dashBoard);

	return (
		<div
			className={
				dashBoard.openDashBoard
					? "fixed h-screen flex-row lg:pr-72 lg:ml-72 ml-80 mt-20 pr-0 duration-300"
					: "fixed h-screen flex-row lg:pr-72 ml-10 mt-20 xs:pr-96 duration-300"
			}
		>
			{props.children}
		</div>
	);
};

export default DashboardDisplayArea;
