import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "../Forms/Button";
import { setContentValues } from "../../redux/emailSlice";

let GigCard = (props) => {
  // VARIABLES
  let gigInfo = props.gigInfo;
  let userAuth = useSelector((state) => state.auth);
  // USESTATE
  let [classname, setClassname] = useState("");
  let [gigUrl, setGigUrl] = useState("");
  let [showMore, setShowMore] = useState(false);
  let [error, setError] = useState(null);

  let gig = {
    gig_id: gigInfo.gig_id,
    admin_id: gigInfo.admin_id,
    brand: gigInfo.brand,
    description: gigInfo.description,
    reward: gigInfo.reward,
    document_ref: gigInfo.document_ref,
    is_open: gigInfo.is_open,
  };

  let handleShowMore = () => {
    setShowMore(!showMore);
    showMore ? setClassname(" max-h-96 z-30 ") : setClassname("");
  };

  let handleFileChange = (e) => {
    if (e.target.value) {
      setGigUrl(e.target.value);
      setError("");
    }
  };

  //   This method is the submit method
  let handleGigSubmittion = () => {
    if (gigUrl === "" || gigUrl === null || gigUrl === undefined) {
      setError("Link is Required *");
      return;
    }

    let email = process.env.REACT_APP_COMPANY_EMAIL;
    let subject = `${userAuth.user.firstName} ${userAuth.user.lastName} submited a Draft for ${gig.brand}`;
    let body = `Hello ${""}, ${userAuth.user.firstName} ${
      userAuth.user.lastName
    } has successfully completed a task assigned by your brand.\n Here are the details of the task:\n- Task ID: ${
      gig.gig_id
    }\n- Brand: ${gig.brand}\n- Description: ${gig.description}\n- Reward: ${
      gig.reward
    }\n- Link: ${gigUrl}\n\nThank you for using our platform!\n\nBest regards,\n[Your Company Name]`;
    let mailtoLink = `mailto:${email}?subject=${encodeURIComponent(
      subject
    )}&body=${encodeURIComponent(body)}`;
    // Sends the email to the to the company email with the details
    window.location.href = mailtoLink;
  };

  return (
    <div className="text-black font-medium">
      <div
        className={`w-72 bg-white rounded-3xl p-4 m-2 text-left justify-evenly ${classname} `}
      >
        <p className="text-2xl font-bold">{gig.brand}</p>
        <p className="font-normal py-2">
          {showMore
            ? gig.description
            : gig.description.length <= 50
            ? gig.description
            : gig.description.substring(0, 30) + "..."}
        </p>
        {/* Format number with .00 */}
        <p className="text-green-600">
          $
          {Intl.NumberFormat("en-US", { minimumFractionDigits: 2 }).format(
            gig.reward
          )}
        </p>
        {/* TODO! For future development */}
        {/* {showMore && file ? (
          <div>
            File details:
            <ul>
              <li>Name: {file.name}</li>
              <li>Type: {file.type}</li>
              <li>Size: {file.size} bytes</li>
            </ul>
          </div>
        ) : null} */}
        {showMore ? (
          <>
            <a href="" className="text-red-700 text-[14px] underline">
              {error}
            </a>

            <input
              id={`text-${gig.gig_id}`}
              className="border-2 border-black rounded-lg pl-[6px] py-[2px] w-full"
              placeholder="Add Link..."
              type="text"
              onChange={handleFileChange}
            />
          </>
        ) : null}
        <Button
          name="Submit"
          onClick={() => {
            handleGigSubmittion();
            handleShowMore();
          }}
          type="submit"
          className="bg-blue-700 text-white"
        />
        <button className="text-blue-400" onClick={handleShowMore}>
          {showMore ? <p>Show Less</p> : <p>Show More</p>}
        </button>
      </div>
    </div>
  );
};

export default GigCard;
