import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import GigCard from "./GigCard";
import UserRequests from "../../utils/userRequests";
import { setGigValues } from "../../redux/auth";

const GigBoard = () => {
  // VARIABLES
  let user = useSelector((state) => state.auth);
  let dispatch = useDispatch();
  // method gets all the active gigs for the user
  let getActiveGigs = async () => {
    var payload = {
      accessToken: user.accessToken,
    };

    await getActiveGigsThunk(payload);
  };

  let getActiveGigsThunk = async (payload) => {
    UserRequests.getActiveGigs(payload)
      .then((data) => {
        dispatch(setGigValues(data["data"]));
      })
      .catch(() => {});
  };

  // USEEFFECT HOOK
  useEffect(() => {
    getActiveGigs();
  }, []);

  return (
    <div className="">
      <div
        id="home-bg"
        className="flex flex-col items-center pt-24 h-screen bg-auto bg-no-repeat"
      >
        <div className=" flex justify-center items-center text-4xl font-bold text-white underline pb-[20px]">
          Gig Board
        </div>
        <div className="scrollbar scrollbar-thumb-transparent scrollbar-track-transparent overflow-y-scroll w-11/12 h-full flex flex-wrap gap-[20px] mt-24 items-center justify-center -translate-y-20 ">
          {user.gigs?.map((gig, index) => (
            <GigCard key={index} gigInfo={gig} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default GigBoard;
