import React, { useState } from "react";
import Button from "../Basic/Button";
import "../Login/Login.css";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import UserRequests from "../../utils/userRequests";
import { setValues } from "../../redux/auth";
import TERMS_AND_CONDITIONS from "../../assets/documents/terms-of-use-zrise.pdf"
import TextInput from "../Components/TextInput";
import { ReactComponent as UserIcon } from "../../assets/icons/person-circle-outline.svg";
import { ReactComponent as MailIcon } from "../../assets/icons/mail-outline.svg";
import { ReactComponent as LockIcon } from "../../assets/icons/lock-closed-outline.svg";
import { ReactComponent as VisibleIcon } from "../../assets/icons/eye-outline.svg";
import { ReactComponent as InvisibleIcon } from "../../assets/icons/eye-off-outline.svg";

let Register = () => {
  // INSTANTIATION 
  let dispatch = useDispatch();
	let navigate = useNavigate();

  // USESTATE 
	let [firstNameText, setFirstNameText] = useState("");
	let [lastNameText, setLastNameText] = useState("");
	let [emailText, setEmailText] = useState("");
	let [passwordText, setPasswordText] = useState("");
	let [confirmPasswordText, setConfirmPasswordText] = useState("");
	let [passwordError, setPasswordError] = useState(false);
	let [error, setError] = useState(false);
  let [agree, setAgree] = useState(false);
  let [passwordVisible, setPasswordVisible] = useState(false);

  // METHODS 
	let onChangeFirstName = (e) => {
		setFirstNameText(e.target.value);
	};

	let onChangeLastName = (e) => {
		setLastNameText(e.target.value);
	};

	let onChangeEmail = (e) => {
		setEmailText(e.target.value);
	};

	let onChangePassword = (e) => {
		setPasswordText(e.target.value);
	};

	let onChangeConfirmPassword = (e) => {
		setConfirmPasswordText(e.target.value);
		if (passwordText !== e.target.value) {
			setPasswordError(true);
		} else {
			setPasswordError(false);
		}
	};

	let createUserThunk = async (payload) => {
		try {
			UserRequests.register(payload)
				.then((data) => {
					// Set session values 
					dispatch(setValues(data["data"]));
					navigate("/login", { state: { key: "registered" } });
				})
				.catch(() => {
					setError(true);
				});
		} catch (e) {
			console.log(e);
		}
	};

	let register = async () => {

		if (passwordText === confirmPasswordText) {
			var payload = {
				firstName: firstNameText,
				lastName: lastNameText,
				email: emailText,
				password: passwordText,
			};
			await createUserThunk(payload);
		} else {
			console.log("Error with Password");
		}
	};

  let handleCheckbox = () => {
    setAgree(!agree)
  }
  // toggle for password visibility
  const handlePasswordIcon = () => {
    setPasswordVisible(!passwordVisible);
  };

	return (
    <div className="w-full h-full">
      <div
        id="login-bg"
        class="w-auto h-screen flex justify-around items-center mx-auto py-24 px-8 bg-auto bg-no-repeat"
      >
        <div className="w-auto h-[500px]flex">
          <div className="flex-col items-center justify-around h-max space-y-[10px]">
            <h1 className="text-3xl font-bold text-center text-white mb-4">
              Register
            </h1>
            <TextInput
                id="firstName"
                type="text"
                placeholder={"John"}
                value={firstNameText}
                onChange={onChangeFirstName}
                containerClassname="border-[1px] border-blue-700 h-max"
                inputIcon={<UserIcon />}
                inputClassname="placeholder-black focus:placeholder-gray-400"
              />
              <TextInput
                id="lastName"
                type="text"
                placeholder={'Doe'}
                value={lastNameText}
                onChange={onChangeLastName}
                containerClassname="border-[1px] border-blue-700 h-max"
                inputIcon={<UserIcon />}
                inputClassname="placeholder-black focus:placeholder-gray-400"
              />
              <TextInput
                id="email"
                type="text"
                onChange={onChangeEmail}
                inputIcon={<MailIcon />}
                placeholder="johndoe@example.com"
                value={emailText}
              />
              <TextInput
                id="password"
                type={passwordVisible ? "text" : "password"}
                onChange={onChangePassword}
                inputIcon={<LockIcon />}
                placeholder="***********"
                value={passwordText}
                iconClick={handlePasswordIcon}
                secondaryIcon={
                  passwordVisible ? <VisibleIcon /> : <InvisibleIcon />
                }
              />
              <TextInput
                id="confirmPassword"
                type={passwordVisible ? "text" : "password"}
                onChange={onChangeConfirmPassword}
                inputIcon={<LockIcon />}
                placeholder="***********"
                value={confirmPasswordText}
              />
            {passwordError ?? (
              <p className="text-xs text-red-600 underline">
                Passwords do not match.
              </p>
            )}
            <div className=" py-[8px] flex items-center ">
              <input type="checkbox" onClick={handleCheckbox}/>
              <a className="ml-[8px] text-blue-400 text-xs underline" href={TERMS_AND_CONDITIONS}>I Agree To Terms & conditions</a>
            </div>
            {agree && <Button type="submit" name="Sign Up" onClick={register} primary />}
            
            
            <a href="/">
            <Button
                type="submit"
                name="Cancel"
                classname='bg-red-700 hover:bg-red-800'
                primary
              />
            </a>
            <a href="/login">
              <p className="text-xs text-blue-400 underline">
                Already have an account?
              </p>
            </a>
            {error ? (
              <p className="text-xs text-red-600 underline">
                Error creating account, please try again later.
              </p>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
