import React from "react";
import { ReactComponent as CloseIcon } from "../../assets/svg/close-outline.svg";

let ModalComp = (props) => {
  return (
    <div className="z-40 top-0 left-0 absolute w-screen h-screen"> {/*bg-black border-2 border-red-700 top-0 left-0 h-screen w-screen absolute opacity-70 flex justify-center*/}
      <div className={`top-0 left-0 z-30 bg-black h-screen w-screen flex items-center opacity-70 absolute ${props.mainClassName}`}></div>
      <div className={`z-30 max-w-[80%] lg:w-fit text-black bg-white flex flex-col justify-center mt-0 p-[20px] rounded-lg absolute ${props.secondClassName}`}>
        <div className="flex justify-between items-center">
          <span className="text-[18px] font-bold">{props.title}</span>
          <CloseIcon
            className="w-[32px] hover:cursor-pointer border-2 border-red-700 rounded-[100px] text-red-700"
            onClick={props.visibility}
          />
        </div>
        <div className="text-black overflow-scroll">{props.component}</div>
      </div>
    </div>
  );
};

export default ModalComp;