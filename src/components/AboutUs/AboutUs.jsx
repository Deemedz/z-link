import React from "react";
import Logo from "../../assets/ZRISE-LOGO.png";

const AboutUs = () => {
  return (
    <div
      name="about"
      className="w-full h-max bg-cover bg-fixed bg-center text-black mb-8 md:mb-0"
    >
      <div className="flex flex-col justify-center items-center w-full h-full bg-white ">
        <div className="max-w-[1080px] w-full grid grid-cols-2 gap-8">
          <div className="sm:text-left pb-8 pl-4">
            <p className="xl:text-6xl md:text-4xl text-3xl font-bold py-4 px-6 w-48">
              About Us
            </p>
          </div>
          <div className=""></div>
        </div>
        <div className="max-w-[1080px] w-full grid grid-cols-1 md:grid-cols-2 gap-8 px-4">
          <div className="font-medium xl:text-xl md:text-lg text-sm px-4">
            <p className="">
              Z-link is the official web-portal of the ZRISE ecosystem. ZRISE
              helps creators manage their digital brands and through Z-link,
              brands have the capacity to collaborate with ZRISE creators on
              digital media projects/campaigns. The platform is still in its
              early stages of development and as a part of its early access
              program, ZRISE is launching a Creator’s Quest. The ZRISE Creator’s
              Quest is a campaign designed to promote, connect and support
              Caribbean digital creators. The quest invites digital content
              creators to compete on a leaderboard through a weekly series of
              content creation challenges using creative briefs from featured
              brands. Participants will have the opportunity to win cash prizes,
              valuable rewards, and even contracts with prominent brands. The
              quest will seamlessly be managed through the user-friendly
              interface of the Z-link web portal.
            </p>
          </div>

          <div className="sm:text-right font-bold">
            <p className="text-4xl">
              <img
                alt="logo"
                src={Logo}
                className="xl:w-[1000px] xl:h-[550px] md:w-[800px] md:h-[500px] w-full h-[400px] "
              />
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

// md:m-10 lg:-translate-y-48 md:-translate-y-32 -translate-y-16

export default AboutUs;
