import React from "react";

const Footer = () => {
	return (
		<div className="w-full">
			<div class="w-auto h-[30px] flex-col-reverse justify-around mx-auto px-8 bg-[#dddddd]">
				<div className="w-auto text-center">
					<p className="font-bold text-lg p-0">
						<a
							href="https://www.zrise.co/"
							target="_blank"
							rel="noopener noreferrer"
						>
							Powered by Z-Rise© 2023
						</a>
					</p>
				</div>
			</div>
		</div>
	);
};

export default Footer;
