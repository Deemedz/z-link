import React, { useState } from "react";
import Button from "../Basic/Button";
import "./Login.css";
import { useDispatch } from "react-redux";
import { setValues } from "../../redux/auth";
import { useNavigate } from "react-router-dom";
import UserRequests from "../../utils/userRequests";
import TextInput from "../Components/TextInput";
import { ReactComponent as MailIcon } from "../../assets/icons/mail-outline.svg";
import { ReactComponent as LockIcon } from "../../assets/icons/lock-closed-outline.svg";
import { ReactComponent as VisibleIcon } from "../../assets/icons/eye-outline.svg";
import { ReactComponent as InvisibleIcon } from "../../assets/icons/eye-off-outline.svg";

const Login = () => {
  // INSTANTIATION
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // USESTATES
  const [emailText, setEmailText] = useState("");
  const [passwordText, setPasswordText] = useState("");
  const [error, setError] = useState(false);
  const [passwordVisible, setPasswordVisible] = useState(false);

  // ONCHANGE
  // for email
  const onChangeEmail = (e) => {
    setEmailText(e.target.value);
  };

  // for password
  const onChangePassword = (e) => {
    setPasswordText(e.target.value);
  };
  // login thunk to send the request
  const loginUserThunk = async (payload) => {
    UserRequests.login(payload)
      .then((data) => {
        if (data.error) {
          setError(true);
        } else {
          dispatch(setValues(data["data"]));
          navigate("/");
        }
      })
      .catch(() => {
        setError(true);
      });
  };

  // login method that creates the payload
  const login = async () => {
    setError(false);
    var payload = {
      email: emailText,
      password: passwordText,
    };
    await loginUserThunk(payload);
  };

  // toggle for password visibility
  const handlePasswordIcon = () => {
    setPasswordVisible(!passwordVisible);
  };

  return (
    <div className="w-full h-full">
      <div
        id="login-bg"
        class="w-auto h-screen flex justify-around items-center mx-auto py-24 px-8 bg-auto bg-no-repeat"
      >
        <div className="flex items-center w-auto h-[500px]">
          <div className="flex-col items-center justify-around">
            <h1 className="text-3xl font-bold text-left text-white mb-[20px]">
              Login
            </h1>
            <div className="flex flex-col gap-[10px]">
              <TextInput
                id="email"
                type="text"
                onChange={onChangeEmail}
                inputIcon={<MailIcon />}
                placeholder="johndoe@example.com"
                value={emailText}
              />
              <TextInput
                id="password"
                type={passwordVisible ? "text" : "password"}
                onChange={onChangePassword}
                inputIcon={<LockIcon />}
                placeholder="***********"
                value={passwordText}
                iconClick={handlePasswordIcon}
                secondaryIcon={
                  passwordVisible ? <VisibleIcon /> : <InvisibleIcon />
                }
              />
            </div>
            <div className="mt-[20px]">
              <a href="/error">
                <p className="text-[10px] text-blue-400 underline">
                  Forgot Your Password?
                </p>
              </a>
              <Button
                type="submit"
                name="Log In"
                onClick={() => {
                  emailText !== "" && passwordText !== ""
                    ? login()
                    : setError(true);
                }}
                primary
              />
              <a href="/register">
                <Button type="submit" name="Sign Up" />
              </a>
            </div>
            {error ? (
              <p className="text-xs text-red-600 underline">
                Error logging in, please try again.
              </p>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
