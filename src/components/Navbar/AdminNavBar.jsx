import React, { useState, useEffect } from "react";
import NavItem from "./NavItem";
import { useLocation, useNavigate } from "react-router-dom";
import Logo from "../../assets/Z-link-Text-Only.png";
import { useDispatch, useSelector } from "react-redux";
import { resetGlobalState } from "../../redux/auth";

let AdminNavBar = () => {
  // VARIABLES
  let location = useLocation();
  let navFalse = () => setNav(false);
  let handleClick = () => setNav(!nav);
  let dispatch = useDispatch();
  let navigate = useNavigate();
  // USESTATE
  let [scrollPosition, setScrollPosition] = useState(0);
  let [nav, setNav] = useState(false);

  // USEEFFECT
  useEffect(() => {
    let handleScroll = () => {
      setScrollPosition(window.scrollY);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  // Logout method
  const logout = () => {
    try {
      let path = "/login/admin";
      dispatch(resetGlobalState());
      return navigate(path);
    } catch (e) {
      console.log(e);
    }
  };
  return (
    <div
      className={
        scrollPosition > 250
          ? "fixed w-full h-[80px] flex justify-between items-center px-4 bg-slate-500/75 text-white duration-500"
          : "fixed w-full h-[80px] flex justify-between items-center px-4 bg-transparent duration-500 text-white"
      }
    >
      <div className={!nav ? "duration-300" : "-translate-x-40 duration-300"}>l
        <a href="/">
          <img src={Logo} alt="Logo" style={{ width: "150px" }} />
        </a>
      </div>
      <ul className="hidden md:flex  text-xl font-medium">
        <NavItem
          name="Users"
          href={location.pathname === "/dashboard" ? false : true}
          linkName="dashboard"
          handleClick={navFalse}
        />
        <NavItem
          name="Gigs"
          href={location.pathname === "/admin/gigs" ? false : true}
          linkName="admin/gigs"
          handleClick={navFalse}
        />
        <NavItem
          name="Logout"
          href={false}
          linkName=""
          handleClick={() => {
            logout();
          }}
        />
      </ul>
    </div>
  );
};

export default AdminNavBar;
