import React from "react";
import { Link } from "react-scroll";

const NavItem = (props) => {
  const name = props.name;
  const linkName = props.linkName;
  const handleClick = props.handleClick;
  const href = props.href;

  return href ? (
    <li className={"py-6 text-xl hover:scale-110 duration-300"}>
      <a href={"/" + linkName} onClick={handleClick}>
        {name}
      </a>
    </li>
  ) : (
    <li className={"py-6 text-xl hover:scale-110 duration-300"}>
      <Link onClick={handleClick} to={linkName} smooth={true} duration={500}>
        {name}
      </Link>
    </li>
  );
};

export default NavItem;
