import React, { useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { FaBars, FaTimes } from "react-icons/fa";
import NavItem from "./NavItem";
import Logo from "../../assets/Z-link-Text-Only.png";
import { useDispatch, useSelector } from "react-redux";
import { resetGlobalState } from "../../redux/auth";

const NavBar = () => {
  let location = useLocation();
  let navigate = useNavigate();
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const [nav, setNav] = useState(false);
  const navFalse = () => setNav(false);
  const handleClick = () => setNav(!nav);

  const [scrollPosition, setScrollPosition] = useState(0);

  // This method logs out the user
  const logout = () => {
    try {
      let path = "/";
      dispatch(resetGlobalState());
      return navigate(path);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    const handleScroll = () => {
      setScrollPosition(window.scrollY);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  // arrays of constant strings
  let gigboardConstants = ["/gig-board", "/login", "/register"];

  return (
    <div
      className={
        scrollPosition > 250
          ? "z-30 fixed w-full h-[80px] flex justify-between items-center px-4 bg-slate-500/75 text-white duration-500"
          : "z-30 fixed w-full h-[80px] flex justify-between items-center px-4 bg-transparent duration-500 text-white"
      }
    >
      {/* Nav Bar Title Logo */}
      <div className={!nav ? "duration-300" : "-translate-x-40 duration-300"}>
        <a href="/">
          <img src={Logo} alt="Logo" style={{ width: "150px" }} />
        </a>
      </div>

      {/* Navigation Desktop View */}
      <ul className="hidden md:flex  text-xl font-medium">
        <NavItem
          name="Home"
          href={location.pathname === "/" ? false : true}
          linkName={location.pathname === "/" ? "home" : ""}
          handleClick={navFalse}
        />
        {location.pathname === "/" ? (
          <>
            <NavItem
              name="About Us"
              href={location.pathname !== "/" ? true : false}
              linkName={location.pathname !== "/" ? "" : "about"}
              handleClick={navFalse}
            />
            <NavItem
              name="Sponsors"
              href={location.pathname !== "/" ? true : false}
              linkName={location.pathname !== "/" ? "" : "sponsor"}
              handleClick={navFalse}
            />
          </>
        ) : null}
        {location.pathname === "/" || location.pathname === "/gig-board" ? (
          <>
            {auth.accessToken ? (
              <NavItem
                name="Profile"
                href={true}
                linkName="profile"
                handleClick={navFalse}
              />
            ) : null}
          </>
        ) : null}
        {!gigboardConstants.includes(location.pathname) ? (
          <>
            {auth.accessToken ? (
              <NavItem
                name="Gig Board"
                href={true}
                linkName="gig-board"
                handleClick={navFalse}
              />
            ) : null}
          </>
        ) : null}

        {location.pathname === "/register" ||
        (location.pathname === "/" && auth.accessToken === "") ? (
          <NavItem
            name="Login"
            href={true}
            linkName="login"
            handleClick={navFalse}
          />
        ) : null}
        {location.pathname === "/login" ||
        (location.pathname === "/" && auth.accessToken === "") ? (
          <NavItem
            name="Register"
            href={true}
            linkName="register"
            handleClick={navFalse}
          />
        ) : null}
        {auth.accessToken !== "" ? (
          <NavItem
            name="Logout"
            href={true}
            linkName=""
            handleClick={() => {
              logout();
            }}
          />
        ) : null}
      </ul>

      {/* Navigation Mobile View */}
      <ul
        className={
          !nav
            ? "hidden -translate-y-40 duration-300 text-xl font-medium"
            : "flex whitespace-nowrap translate-y-0 -translate-x-20 duration-300 text-xl font-medium"
        }
      >
        {location.pathname === "/" ? (
          <NavItem
            name="Home"
            href={false}
            linkName="home"
            handleClick={navFalse}
          />
        ) : (
          <NavItem name="Home" href={true} linkName="" handleClick={navFalse} />
        )}
        {location.pathname === "/" ? (
          <NavItem
            name="About Us"
            href={false}
            linkName="about"
            handleClick={navFalse}
          />
        ) : null}
        {location.pathname === "/register" || location.pathname === "/" ? (
          <NavItem
            name="Login"
            href={true}
            linkName="login"
            handleClick={navFalse}
          />
        ) : location.pathname === "/login" ? (
          <NavItem
            name="Register"
            href={true}
            linkName="register"
            handleClick={navFalse}
          />
        ) : (
          <NavItem
            name="Profile"
            href={true}
            linkName="profile"
            handleClick={navFalse}
          />
        )}
        {location.pathname != "/register" &&
        location.pathname != "/" &&
        location.pathname != "/login" ? (
          <NavItem name="Logout" href={true} linkName="" handleClick={logout} />
        ) : null}
      </ul>

      {/* Hamburger Menu */}
      <div
        onClick={handleClick}
        className={
          !nav
            ? "md:hidden z-30 -translate-x-0 duration-300"
            : "md:hidden z-30 -translate-x-0 duration-300"
        }
      >
        {!nav ? (
          <FaBars className="text-2xl border-2" />
        ) : (
          <FaTimes className="text-red-400 text-2xl" />
        )}
      </div>
    </div>
  );
};

export default NavBar;
