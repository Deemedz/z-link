import React, { useEffect } from "react";
import "./Table.css";
import TableRow from "./TableRow.jsx";
import TableHeadings from "./TableHeadings.jsx";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const GigTable = (props) => {
	const headings = props.headings;
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const auth = useSelector((state) => state.auth);
	var gigs = [];

	useEffect(() => {
		console.log(auth.accessToken);
		axios.defaults.headers.common["Authorization"] =
			"Bearer " + auth.accessToken;

		try {
			axios
				.get(process.env.REACT_APP_BASE_URL + "/jobs")
				.then((res) => {
					if (res.data.error) {
						navigate("/error");
					} else {
						if (res.data.data == null) {
							var emptyGig = {
								gig_id: 0,
								admin_id: 0,
								brand: "N/A",
								description: "No gigs in database",
								reward: "0",
								document_ref: null,
								is_open: false,
							};

							this.setState(() => gigs.concat(emptyGig));
						} else {
							console.log("data found");
						}
					}
				})
				.catch((err) => {
					console.log(err);
				});
		} catch (e) {
			console.log(e);
		}
	});

	const entries = [
		{ name: "Batman", client: "Bruce Wayne", status: "Active" },
		{ name: "Robin", client: "Dick Grayson", status: "Current" },
		{ name: "Batgirl", client: "Barbara Gordon", status: "Completed" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
		{ name: "Batman", client: "Bruce Wayne", status: "Active" },
		{ name: "Robin", client: "Dick Grayson", status: "Current" },
		{ name: "Batgirl", client: "Barbara Gordon", status: "Completed" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
		{ name: "Batman", client: "Bruce Wayne", status: "Active" },
		{ name: "Robin", client: "Dick Grayson", status: "Current" },
		{ name: "Batgirl", client: "Barbara Gordon", status: "Completed" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
	];

	return (
		<div className="w-full h-[80%] lg:mx-[100%] bg-transparent overflow-scroll">
			<div className="flex justify-center items-center text-4xl font-bold text-white underline">
				Gigs Table
			</div>
			<div id="table-body" class="flex justify-center items-center">
				<div class="container">
					<table class="w-full flex flex-row flex-no-wrap sm:bg-white rounded-lg overflow-hidden sm:shadow-lg my-5">
						<thead class="text-white">
							{entries.map((entry) => {
								return <TableHeadings headings={headings} />;
							})}
						</thead>
						<tbody class="flex-1 sm:flex-none">
							{entries.map((entry) => {
								return (
									<TableRow
										name={entry.name}
										client={entry.client}
										status={entry.status}
									/>
								);
							})}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	);
};

export default GigTable;
