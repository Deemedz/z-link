import React, { useEffect } from "react";
import "./Table.css";
import TableRow from "./TableRow.jsx";
import TableHeadings from "./TableHeadings.jsx";

const UserTable = (props) => {
	const headings = props.headings;

	const entries = [
		{ name: "Batman", client: "Bruce Wayne", status: "Active" },
		{ name: "Robin", client: "Dick Grayson", status: "Current" },
		{ name: "Batgirl", client: "Barbara Gordon", status: "Completed" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
		{ name: "Batman", client: "Bruce Wayne", status: "Active" },
		{ name: "Robin", client: "Dick Grayson", status: "Current" },
		{ name: "Batgirl", client: "Barbara Gordon", status: "Completed" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
		{ name: "Batman", client: "Bruce Wayne", status: "Active" },
		{ name: "Robin", client: "Dick Grayson", status: "Current" },
		{ name: "Batgirl", client: "Barbara Gordon", status: "Completed" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
		{ name: "The Flash", client: "Barry Allen", status: "Pending" },
	];

	return (
		<div className="w-full h-[80%] lg:mx-[100%] bg-transparent overflow-scroll">
			<div className="flex justify-center items-center text-4xl font-bold text-white underline">
				User Table
			</div>
			<div id="table-body" class="flex justify-center items-center">
				<div class="container">
					<table class="w-full flex flex-row sm:bg-white rounded-lg overflow-hidden sm:shadow-lg my-5">
						<thead class="text-white">
							{entries.map((entry) => {
								return <TableHeadings headings={headings} />;
							})}
						</thead>
						<tbody class="flex-1 sm:flex-none">
							{entries.map((entry) => {
								return (
									<TableRow
										name={entry.name}
										client={entry.client}
										status={entry.status}
									/>
								);
							})}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	);
};

export default UserTable;
