import React from "react";

const TableRow = (props) => {
	const name = props.name;
	const email = props.client;
	const status = props.status;

	return (
		<tr class="flex flex-col flex-no-wrap bg-white sm:table-row mb-1">
			<td class="border-grey-light border hover:bg-gray-100 p-3">{name}</td>
			<td class="border-grey-light border hover:bg-gray-100 p-3 truncate">
				{email}
			</td>
			<td class="border-grey-light border hover:bg-gray-100 p-3 truncate">
				{status}
			</td>
			<td class="border-grey-light border hover:bg-gray-100 p-3 truncate hover:font-medium cursor-pointer">
				<div class="pb-[9px] flex item-center justify-center">
					<div class="w-4 mr-2 transform hover:scale-150 duration-300  text-blue-400 hover:text-blue-600 ">
						<svg
							xmlns="http://www.w3.org/2000/svg"
							fill="none"
							viewBox="0 0 24 24"
							stroke="currentColor"
						>
							<path
								stroke-linecap="round"
								stroke-linejoin="round"
								stroke-width="2"
								d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
							/>
						</svg>
					</div>
					<div class="w-4 mr-2 transform hover:scale-150 duration-300  text-red-400 hover:text-red-600 ">
						<svg
							xmlns="http://www.w3.org/2000/svg"
							fill="none"
							viewBox="0 0 24 24"
							stroke="currentColor"
						>
							<path
								stroke-linecap="round"
								stroke-linejoin="round"
								stroke-width="2"
								d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
							/>
						</svg>
					</div>
				</div>
			</td>
		</tr>
	);
};

export default TableRow;

{
	/* 
Add Image to cell in Row

<div class="mr-2">
    <img class="w-6 h-6 rounded-full" src="https://randomuser.me/api/portraits/men/1.jpg"/>
</div>

*/
}
