// TODO!!! Update the styling of the table

import React, { useState } from "react";
import { ReactComponent as EditIcon } from "../../assets/svg/edit-icon.svg";
import ModalComp from "../Modal/ModalComp";
import GigForm from "../Forms/GigForm";
import { useLocation, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setGigValues } from "../../redux/auth";
import EditGigForm from "../Forms/EditGigForm";
import { setSelectedGig } from "../../redux/adminAuth";

const Table = (props) => {
  // VARIABLES
  let location = useLocation();
  let dispatch = useDispatch();
  // USESTATE
  let [isModalVisible, setIsModalVisible] = useState(false);
  let [isEditModalVisible, setIsEditModalVisible] = useState(false);
  let [isClosed, setIsClosed] = useState("0");

  // This method updates the state for the modal's visibility
  let updateModalVisibility = () => {
    setIsModalVisible(!isModalVisible);
  };

  let updateEditModalVisibility = () => {
    setIsEditModalVisible(!isEditModalVisible);
  };

  // places the component in a variable to pass
  // let GigFormComponent = <GigForm onCancelClick={updateModalVisibility} />;
  // The following ensures the Object.Key method
  // is not called on an empty object which inturn
  // prevents a logic error.
  if (props.headers === undefined || props.headers === null) {
    return <p>No data available</p>;
  }
  // variable for the headers
  let head = Object.keys(props.headers);

  // this method replaces the underscores from the headings
  // with spaces and replaces all the first letter of wach
  // letter with its capital counterpart.
  let headingFormatter = (value) => {
    return value
      .replace("_", " ")
      .replace(/\b\w/g, (char) => char.toUpperCase());
  };

  let handleDispatch = (record) => {
    let payload = {
      gig: record,
    };
    dispatch(setSelectedGig(payload));
  };

  let EditGigFormComponent = (
    <EditGigForm
      onConfirmClick={() => {
        updateEditModalVisibility();
      }}
      onClickGetData={() => {
        props.onClickGetData();
      }}
      onCancelClick={updateEditModalVisibility}
    />
  );
  return (
    <div className="w-full md:w-10/12 bg-white p-[20px] rounded-xl overflow-x-scroll">
      <div className="w-full flex flex-row items-center justify-between gap-[20px] pb-[10px]">
        <h1 className="text-neutral-600 text-[18px] md:text-[24px] font-normal  text-nowrap">
          {props.title}
        </h1>
        {location.pathname === "/admin/gigs" ? (
          <div className="flex flex-row items-center justify-between gap-[10px]">
            <span
              onClick={() => {
                setIsClosed("0");
              }}
              className="border-[1px] border-neutral-200 px-4 py-[1px] rounded-[50px] hover:cursor-pointer text-[16px] sm:text-[12px]"
            >
              All
            </span>
            <span
              onClick={() => {
                setIsClosed("1");
              }}
              className="border-[1px] border-neutral-200 px-4 py-[1px] rounded-[50px] hover:cursor-pointer text-[16px] sm:text-[12px]"
            >
              Active
            </span>
            <span
              onClick={() => {
                setIsClosed("2");
              }}
              className="border-[1px] border-neutral-200 px-4 py-[1px] rounded-[50px] hover:cursor-pointer text-[16px] sm:text-[12px]"
            >
              Closed
            </span>
            {props.icon !== null ? (
              <div className="border-[1px] border-neutral-200 h-[20px] w-[20px] md:h-[24px] md:w-[24px] rounded-[100px] hover:cursor-pointer">
                {props.icon}
              </div>
            ) : null}
          </div>
        ) : null}
      </div>
      <table className="w-full sm:text-[12px] lg:text-[16px]">
        <thead className="bg-neutral-100">
          <tr className=" text-neutral-600 last:border-none">
            {head.map(
              (key) =>
                key !== "document_ref" && (
                  <th className="p-2 text-nowrap border-[1px] border-neutral-200 font-light">
                    {headingFormatter(key)}
                  </th>
                )
            )}
          </tr>
        </thead>
        <tbody className="bg-neutral-50">
          {props.rows !== null || props.rows !== undefined
            ? props.rows?.map((record, index) =>
                isClosed === "0" ? (
                  <>
                    <tr
                      key={index}
                      className=" text-neutral-600 font-extralight text-[12px]"
                    >
                      {Object.keys(record).map(
                        (key) =>
                          key !== "document_ref" && (
                            <td className="border-[1px] border-neutral-200 my-auto">
                              <span
                                href={null}
                                onClick={() => {
                                  handleDispatch(record);
                                  updateEditModalVisibility();
                                }}
                                className={`flex justify-center hover:cursor-pointer ${key === "is_open" ? 'border-[1px] w-max rounded-xl px-[8px] m-auto' : null} ${key === "is_open" ? record[key] ? 'bg-green-400' : 'bg-red-400' : null}`}
                              >
                                {key === "is_open"
                                  ? record[key]
                                    ? "Active"
                                    : "Closed"
                                  : record[key]}
                              </span>
                            </td>
                          )
                      )}
                    </tr>
                  </>
                ) : isClosed === "1" && record.is_open ? (
                  <tr
                    key={index}
                    className=" text-neutral-600 font-extralight text-[12px]"
                  >
                    {Object.keys(record).map(
                      (key) =>
                        key !== "document_ref" && (
                          <td className="border-[1px] border-neutral-200 my-auto">
                            <span
                              href={null}
                              onClick={() => {
                                handleDispatch(record);
                                updateEditModalVisibility();
                              }}
                              className={`flex justify-center hover:cursor-pointer ${key === "is_open" ? 'border-[1px] w-max rounded-xl px-[8px] m-auto' : null} ${key === "is_open" ? record[key] ? 'bg-green-400' : 'bg-red-400' : null}`}
                            >
                              {key === "is_open"
                                ? record[key]
                                  ? "Active"
                                  : "Closed"
                                : record[key]}
                            </span>
                          </td>
                        )
                    )}
                  </tr>
                ) : isClosed === "2" && record.is_open === false ? (
                  <tr
                    key={index}
                    className=" text-neutral-600 font-extralight text-[12px]"
                  >
                    {Object.keys(record).map(
                      (key) =>
                        key !== "document_ref" && (
                          <td className="border-[1px] border-neutral-200 my-auto">
                            <span 
                              href={null}
                              onClick={() => {
                                handleDispatch(record);
                                updateEditModalVisibility();
                              }}
                              className={`flex justify-center hover:cursor-pointer ${key === "is_open" ? 'border-[1px] w-max rounded-xl px-[8px] m-auto' : null} ${key === "is_open" ? record[key] ? 'bg-green-400' : 'bg-red-400' : null}`}
                            >
                              {key === "is_open"
                                ? record[key]
                                  ? "Active"
                                  : "Closed"
                                : record[key]}
                            </span>
                          </td>
                        )
                    )}
                  </tr>
                ) : null
              )
            : null}
        </tbody>
      </table>
      {location.pathname === "/admin/gigs"
        ? isEditModalVisible && (
            <ModalComp
              title="Gig Update"
              secondClassName="relative m-auto min-w-[30%] mt-[12%]"
              component={EditGigFormComponent}
              visibility={() => {
                updateEditModalVisibility();
              }}
            />
          )
        : null}
    </div>
  );
};

export default Table;
