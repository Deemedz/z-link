// TODO! User should be able to update password.
// TODO! Make edit modal mobile responsive
// TODO! Allow user to update profile FaPhotoVideo, this requires backend work aswell
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ReactComponent as EditIcon } from "../../assets/svg/edit-icon.svg";
import Button from "../Basic/Button";
import SocialsTable from "./Socials/SocialsTable";
import UserRequests from "../../utils/userRequests";
import { setUpdatedValues } from "../../redux/auth";
import TextInput from "../Components/TextInput";
import TextArea from "../Components/TextArea";
import { ReactComponent as UserIcon } from "../../assets/icons/person-circle-outline.svg";
import { ReactComponent as MailIcon } from "../../assets/icons/mail-outline.svg";
import { ReactComponent as PhoneIcon } from "../../assets/icons/phone-portrait-outline.svg";
import { ReactComponent as BioIcon } from "../../assets/icons/create-outline.svg";
import { ReactComponent as LockIcon } from "../../assets/icons/lock-closed-outline.svg";
import { ReactComponent as VisibleIcon } from "../../assets/icons/eye-outline.svg";
import { ReactComponent as InvisibleIcon } from "../../assets/icons/eye-off-outline.svg";

let Profile = () => {
  // INSTANTIATION
  let user = useSelector((state) => state.auth);
  let dispatch = useDispatch();

  // USE STATE
  let [modalOpen, setModalOpen] = useState(false);
  let [socialModalOpen, setSocialModalOpen] = useState(false);
  let [error, setError] = useState(false);
  let [passwordVisible, setPasswordVisible] = useState(false);
  let [firstNameText, setFirstNameText] = useState("");
  let [lastNameText, setLastNameText] = useState("");
  let [emailText, setEmailText] = useState("");
  let [passwordText, setPasswordText] = useState("");
  let [confirmPasswordText, setConfirmPasswordText] = useState("");
  let [phoneText, setPhoneText] = useState("");
  let [bioText, setBioText] = useState("");
  let [file, setFile] = useState(null);

  let openModal = () => {
    setModalOpen(true);
  };

  let closeModal = () => {
    setModalOpen(false);
  };

  let openSocialModal = () => {
    setSocialModalOpen(true);
  };

  let onChangeFirstName = (e) => {
    let text = onCapitalizeFirstLetter(e.target.value);
    setFirstNameText(text);
  };

  let onChangeLastName = (e) => {
    let text = onCapitalizeFirstLetter(e.target.value);
    setLastNameText(text);
  };

  let onChangeEmail = (e) => {
    setEmailText(e.target.value);
  };

  let onChangePhone = (e) => {
    setPhoneText(e.target.value);
  };

  let onChangeBio = (e) => {
    setBioText(e.target.value);
  };

  let onChangePassword = (e) => {
    setPasswordText(e.target.value);
  };

  let onChangeConfirmPassword = (e) => {
    setConfirmPasswordText(e.target.value);
  };

  // toggle for password visibility
  let onPasswordIcon = () => {
    setPasswordVisible(!passwordVisible);
  };

  // Takes the information submitted in the form
  // for the user information to be updated
  let onUpdateUser = async () => {
    setFirstNameText(onCapitalizeFirstLetter(firstNameText));
    setLastNameText(onCapitalizeFirstLetter(lastNameText));
    var payload = {
      userId: user.user.userId,
      firstName: firstNameText === "" ? user.user.firstName : firstNameText,
      lastName: lastNameText === "" ? user.user.lastName : lastNameText,
      email: emailText === "" ? user.user.emailAddress : emailText,
      phone: phoneText === "" ? user.user.phoneNumber : phoneText,
      bio: bioText === "" ? user.user.bio : bioText,
      accessToken: user.accessToken,
    };
    await onUpdateUserThunk(payload);
  };

  //  thunk method to call the request methods
  let onUpdateUserThunk = async (payload) => {
    UserRequests.update(payload)
      .then((data) => {
        setError(false);
        dispatch(setUpdatedValues(data["data"]));
        closeModal();
      })
      .catch((e) => {
        setError(true);
      });
  };
  // capitalizes the first letter of the word
  const onCapitalizeFirstLetter = (word) => {
    return word.charAt(0).toUpperCase() + word.slice(1);
  };

  // TODO! future password updates to be made
  // to initiate password update
  // let onUpdatePassword = () => {
  //   if(passwordText === '' || passwordText.length < 8){
  //     setError(true);
  //     return;
  //   }
  //   setError(false);
  //   let payload = {
  //     userId: user.user.userId,
  //     accessToken: user.accessToken,
  //     password: passwordText,
  //   };
  //   await onUpdatePasswordThunk(payload)
  // }

  // let onUpdatePasswordThunk = async (payload) => {
  //   UserRequest.updatePassword(payload)
  // }

  // TODO: Future task
  // const handleFileChange = (e) => {
  //   if (e.target.files) {
  //     setFile(e.target.files[0]);
  //   }
  // };

  return (
    <div className="w-full h-full">
      <dialog
        open={modalOpen}
        className=" h-full w-full bg-black bg-opacity-80 z-40"
      >
        <div className=" z-50 w-[400px] sm:w-[750px] h-max sm:h-max flex-col items-center justify-center bg-white container rounded-3xl pb-[20px]">
          <p className="text-center text-3xl font-medium py-4 my-4 mt-12 pt-8">
            {error ? (
              <p className="text-lg text-red-600 underline">
                Error Updating account, please try again later.
              </p>
            ) : null}
            Edit Profile Details
          </p>
          <form method="dialog">
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-x-4 gap-y-2 place-items-center">
              <TextInput
                id="firstName"
                type="text"
                placeholder={user.user.firstName}
                value={firstNameText}
                onChange={onChangeFirstName}
                containerClassname="border-[1px] border-blue-700 h-max"
                inputIcon={<UserIcon />}
                inputClassname="placeholder-black focus:placeholder-gray-400"
              />
              <TextInput
                id="lastName"
                type="text"
                placeholder={user.user.lastName}
                value={lastNameText}
                onChange={onChangeLastName}
                containerClassname="border-[1px] border-blue-700 h-max"
                inputIcon={<UserIcon />}
                inputClassname="placeholder-black focus:placeholder-gray-400"
              />
              <TextInput
                id="email"
                type="text"
                placeholder={user.user.emailAddress}
                value={emailText}
                onChange={onChangeEmail}
                containerClassname="border-[1px] border-blue-700 h-max"
                inputIcon={<MailIcon />}
                inputClassname="placeholder-black focus:placeholder-gray-400"
              />
              <TextInput
                id="phone"
                type="text"
                placeholder={user.user.phoneNumber ?? "(xxx) xxx-xxxx"}
                value={phoneText}
                onChange={onChangePhone}
                containerClassname="border-[1px] border-blue-700 h-max"
                inputIcon={<PhoneIcon />}
                inputClassname="placeholder-black focus:placeholder-gray-400"
              />
              <TextArea
                id={"bio"}
                type={"text"}
                onChange={onChangeBio}
                placeholder={user.user.bio ?? "Bio"}
                value={bioText}
                textareaIcon={<BioIcon />}
                textareaClassname="placeholder-black"
                length={50}
              />
              {/* <TextInput
                id="password"
                type={"text" }
                onChange={onChangePassword}
                inputIcon={<LockIcon />}
                placeholder={user.user.password}
                value={passwordText}
                iconClick={onPasswordIcon}
                secondaryIcon={
                  passwordVisible ? <VisibleIcon /> : <InvisibleIcon />
                }
                containerClassname="border-[1px] border-blue-700 h-max"
              /> */}
            </div>
            <div className="mt-[20px] grid grid-cols-1 lg:grid-cols-2 gap-x-4 gap-y-2">
              {/* <Button
                name="Set Socials"
                onClick={() => {
                  closeModal();
                  openSocialModal();
                }}
                primary
                type="Button"
              /> */}
              {/* <Button
                name="Change Password"
                onClick={() => {
                  // onUpdatePassword();
                }}
                primary
                type="Button"
              /> */}
            </div>
            <Button
              name="Submit"
              onClick={() => {
                onUpdateUser();
              }}
              primary
              type="Button"
            />
            <Button name="Close" onClick={closeModal} type="Button" />
          </form>
        </div>
      </dialog>

      <dialog
        open={socialModalOpen}
        className="h-full w-full bg-black bg-opacity-50"
      >
        <div className="z-50 w-[400px] sm:w-[900px] h-[800px] sm:h-[500px] flex-col items-center justify-center bg-white container rounded-3xl">
          <SocialsTable />
        </div>
      </dialog>

      <div
        id="home-bg"
        className="w-screen h-screen flex justify-around items-center mx-auto py-24 px-8 bg-auto bg-no-repeat"
      >
        {!modalOpen && !socialModalOpen && (
          <div className="w-max h-[500px] items-center justify-center -translate-y-20 text-white mt-40">
            <div className="font-medium flex justify-end">
              <button
                className=" flex items-center bg-white rounded-[100px] px-2"
                onClick={openModal}
              >
                <p className=" text-black">Edit</p>
                <EditIcon className="w-[24px] text-white" />
              </button>
            </div>
            <div className="z-1">
              <img
                src="https://picsum.photos/400"
                className=" object-cover mx-auto my-2 w-56 h-56 sm:w-64 sm:h-64 rounded-full bg-white overflow-hidden"
              />

              <p className="text-3xl sm:text-4xl font-bold text-center my-2 ">
                {user.user.firstName + " " + user.user.lastName}
              </p>
            </div>
            <div className="relative">
              <p className="text-xl sm:text-2xl font-normal text-center my-4 ">
                {user.user.bio ? user.user.bio : <p>Bio</p>}
              </p>
            </div>
            {/* TODO: Future fixes  */}
            {/* <div className="relative">
              <p className="text-xl sm:text-2xl font-normal text-center my-4">
                <p>socials</p>
              </p>
            </div> */}
          </div>
        )}
      </div>
    </div>
  );
};

export default Profile;
