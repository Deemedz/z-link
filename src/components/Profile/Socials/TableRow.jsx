import React, { useState } from "react";
import TextBox from "../../Basic/TextBox";

const TableRow = (props) => {
	const name = props.socialName;
	const link = props.socialLink;
	const socialId = props.socialId;
	const userId = props.userId;
	const newSocial = props.newSocial;

	const [nameText, setNameText] = useState(name);
	const [linkText, setLinkText] = useState(link);

	const onChangeName = (e) => {
		setNameText(e.target.value);
	};
	const onChangeLink = (e) => {
		setNameText(e.target.value);
	};

	return (
		<tr class="flex flex-col flex-no-wrap bg-white sm:table-row mb-1 w-44">
			<td class="border-grey-light border hover:bg-gray-100 pl-2 sm:w-72 p-0">
				<TextBox name="Social Name" onChange={onChangeName} value={name} />
			</td>
			<td class="border-grey-light border hover:bg-gray-100 pl-2 truncate sm:w-72 p-0">
				<TextBox name="Social Link" onChange={onChangeLink} value={link} />
			</td>
			{/* //TODO! Create a dropdown for the social names and have the social link be editable. Change the action to only delete and add a submit button*/}
			<td class="border-grey-light border hover:bg-gray-100 p-3 truncate hover:font-medium cursor-pointer w-44 h-12 mb-0.5">
				{newSocial ? (
					<div class="pb-[9px] flex item-center justify-center">
						<div class="w-6 mr-2 transform hover:scale-150 hover:translate-x-1 translate-x-1 translate-y-1 duration-300  text-green-600 hover:text-green-500 ">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								fill="#4feb34"
								viewBox="0 0 512 512"
								stroke="currentColor"
							>
								<path
									xmlns="http://www.w3.org/2000/svg"
									d="M256,11C120.9,11,11,120.9,11,256s109.9,245,245,245s245-109.9,245-245S391.1,11,256,11z M256,460.2    c-112.6,0-204.2-91.6-204.2-204.2S143.4,51.8,256,51.8S460.2,143.4,460.2,256S368.6,460.2,256,460.2z"
								/>
								<path
									xmlns="http://www.w3.org/2000/svg"
									d="m357.6,235.6h-81.2v-81.2c0-11.3-9.1-20.4-20.4-20.4-11.3,0-20.4,9.1-20.4,20.4v81.2h-81.2c-11.3,0-20.4,9.1-20.4,20.4s9.1,20.4 20.4,20.4h81.2v81.2c0,11.3 9.1,20.4 20.4,20.4 11.3,0 20.4-9.1 20.4-20.4v-81.2h81.2c11.3,0 20.4-9.1 20.4-20.4s-9.1-20.4-20.4-20.4z"
								/>
							</svg>
						</div>
					</div>
				) : (
					<div class="pb-[9px] flex item-center justify-center">
						<div class="w-4 mr-2 transform hover:scale-150 duration-300  text-blue-400 hover:text-blue-600 ">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								fill="none"
								viewBox="0 0 24 24"
								stroke="currentColor"
							>
								<path
									stroke-linecap="round"
									stroke-linejoin="round"
									stroke-width="2"
									d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
								/>
							</svg>
						</div>
						<div class="w-4 mr-2 transform hover:scale-150 duration-300  text-red-400 hover:text-red-600 ">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								fill="none"
								viewBox="0 0 24 24"
								stroke="currentColor"
							>
								<path
									stroke-linecap="round"
									stroke-linejoin="round"
									stroke-width="2"
									d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
								/>
							</svg>
						</div>
					</div>
				)}
			</td>
		</tr>
	);
};

export default TableRow;

{
	/* 
Add Image to cell in Row

<div class="mr-2">
    <img class="w-6 h-6 rounded-full" src="https://randomuser.me/api/portraits/men/1.jpg"/>
</div>

*/
}
