import React from "react";

const TableHeadings = (props) => {
	const headings = props.headings;

	return (
		<tr class="bg-teal-400 flex flex-col flex-no-wrap sm:table-row rounded-l-lg sm:rounded-none mb-1">
			{headings.map((heading) => {
				return <th class="p-3 text-left">{heading}</th>;
			})}
		</tr>
	);
};

export default TableHeadings;
