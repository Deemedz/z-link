import React from "react";
import TableRow from "./TableRow";
import TableHeadings from "./TableHeadings";

const SocialsTable = () => {
	const socialHeadings = ["Social Name", "Social Link", "Actions"];

	var entries = [
		{
			name: "LinkedIn",
			link: "www.linkedIn.com",
			id: 1,
			userId: 1,
		},
		{
			name: "FaceBook",
			link: "www.facebook.com",
			id: 2,
			userId: 1,
		},
		{
			name: "LinkedIn",
			link: "www.linkedIn.com",
			id: 3,
			userId: 1,
		},
		{
			name: "FaceBook",
			link: "www.facebook.com",
			id: 4,
			userId: 1,
		},
		{
			name: "LinkedIn",
			link: "www.linkedIn.com",
			id: 5,
			userId: 1,
		},
		{
			name: "FaceBook",
			link: "www.facebook.com",
			id: 6,
			userId: 1,
		},
		{
			name: "LinkedIn",
			link: "www.linkedIn.com",
			id: 7,
			userId: 1,
		},
		{
			name: "FaceBook",
			link: "www.facebook.com",
			id: 8,
			userId: 1,
		},
	];

	return (
		<div className="w-full h-full bg-transparent overflow-scroll">
			<div className="flex justify-center items-center text-4xl font-bold underline">
				Socials Table
			</div>
			<div id="table-body" class="flex justify-center items-center">
				<div class="container">
					<table class="w-full flex flex-row flex-no-wrap sm:bg-white rounded-lg overflow-scroll sm:shadow-lg my-5">
						<thead class="text-white">
							{entries.map((entry) => {
								return <TableHeadings headings={socialHeadings} />;
							})}
							<TableHeadings headings={socialHeadings} />;
						</thead>
						<tbody class="flex-1 sm:flex-none">
							{entries.map((entry) => {
								return (
									<TableRow
										socialName={entry.name}
										socialId={entry.id}
										userId={entry.userId}
										socialLink={entry.link}
									/>
								);
							})}
							<TableRow socialName={""} socialLink={""} newSocial={true} />
						</tbody>
					</table>
				</div>
			</div>
		</div>
	);
};

export default SocialsTable;
