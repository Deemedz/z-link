import React from "react";

const Button = (props) => {
  return (
    <button
      type={props.type}
      onClick={props.onClick}
      className={` border-solid shadow-xl sm:my-2 hover:shadow-2xl focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-xl text-sm w-full w-auto px-5 py-2.5 text-center my-4 duration-300 ${props.className}`}
    >
      {props.name}
    </button>
  );
};

export default Button;
