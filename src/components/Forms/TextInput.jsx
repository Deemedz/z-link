import React from "react";

const TextInput = (props) => {
	return (
    <div className="text-black">
      <input
        onChange={props.onChange}
        type={props.type}
        id={props.id}
        className={`my-4 sm:my-2 duration-300 shadow-xl text-sm rounded-lg block p-2.5 ${props.classNames}`}
        placeholder={props.placeholder}
        value={props.value ? props.value : null}
        required={props.required ? true : false}
        step={props.id === "currencyInput" ? "0.01" : "1" }
      />
      {/*
  <p class={props.error ? "mt-2 text-sm text-red-600 dark:text-red-500" : "hidden"}>Email Address or Password is incorrect</p>*/}
    </div>
  );
};

export default TextInput;
