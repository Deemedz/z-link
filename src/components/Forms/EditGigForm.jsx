import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "./Button";
import AdminRequests from "../../utils/adminRequests";

let EditGigForm = (props) => {
  // INSTANTIATION
  let admin = useSelector((state) => state.adminAuth);
  // USESTATE
  let [brandValue, setBrandValue] = useState("");
  let [rewardValue, setRewardValue] = useState("");
  let [descriptionValue, setDescriptionValue] = useState("");
  let [isChecked, setIsChecked] = useState(admin.gig.is_open);

  // ONCHANGE
  let onChangeBrand = (e) => {
    setBrandValue(e.target.value);
  };
  let onChangeReward = (e) => {
    setRewardValue(e.target.value);
  };
  let onChangeDescription = (e) => {
    setDescriptionValue(e.target.value);
  };

  //   HANDLERS
  //   main update function
  let handleUpdate = () => {
    handleDetailUpdate();
    isChecked !== admin.gig.is_open && handleStatusUpdate();
    props.onConfirmClick();
  };

  //   handles the brand, reward and description updates
  let handleDetailUpdate = async () => {
    let payload = {
      gig_id: admin.gig.gig_id,
      brand: brandValue === "" ? admin.gig.brand : brandValue,
      description:
        descriptionValue === "" ? admin.gig.description : descriptionValue,
      reward: rewardValue === "" ? admin.gig.reward : rewardValue,
      accessToken: admin.accessToken,
    };
    await handleDetailUpdateThunk(payload);
  };
  //   used to send update request
  let handleDetailUpdateThunk = async (payload) => {
    AdminRequests.updateGig(payload)
      .then((data) => {
        props.onClickGetData();
      })
      .catch((error) => {
      });
  };

  //   creates payload for status update
  let handleStatusUpdate = async () => {
    let payload = {
      gig_id: admin.gig.gig_id,
      status: isChecked,
      accessToken: admin.accessToken,
    };

    await handleStatusUpdateThunk(payload);
  };

  //   used to send status update request
  let handleStatusUpdateThunk = async (payload) => {
    AdminRequests.updateStatus(payload)
      .then((data) => {
        props.onClickGetData();
      })
      .catch(() => {});
  };

  //   handles the toggle of the switch
  let handleToggle = () => {
    setIsChecked(!isChecked);
  };

  return (
    <div className="my-[10px]">
      <div className=" grid md:grid-rows-2 gap-[10px]">
        <input
          id="brand"
          type="text"
          className={`border-2 shadow-inner my-4 sm:my-2 duration-300 text-sm rounded-lg block p-2.5  ${props.classNames}`}
          placeholder={admin.gig.brand}
          value={brandValue}
          onChange={onChangeBrand}
        />
        <input
          id="reward"
          type="number"
          className={`my-4 sm:my-2 duration-300 border-2 shadow-inner text-sm rounded-lg block p-2.5  ${props.classNames}`}
          placeholder={Intl.NumberFormat("en-US", {
            minimumFractionDigits: 2,
          }).format(admin.gig.reward)}
          value={rewardValue}
          onChange={onChangeReward}
        />
        <textarea
          id="description"
          placeholder={admin.gig.description}
          value={descriptionValue}
          onChange={onChangeDescription}
          className="my-4 sm:my-2 duration-300 border-2 shadow-inner text-sm rounded-lg block p-2.5 w-full"
        />
        <div className="flex flex-col items-center w-max hover:cursor-pointer">
          <span className="text-[12px]">Status</span>
          <label className={`switch round ${isChecked ? "checked" : ""}`}>
            <input
              type="checkbox"
              onChange={handleToggle}
              checked={isChecked}
            />
            <span className="slider round bg-gray-400"></span>
          </label>
        </div>
        <div className="flex flex-row gap-[20px]">
          <Button
            type={"submit"}
            onClick={() => {
              handleUpdate();
            }}
            name="Update"
            className="text-white bg-blue-700 hover:bg-blue-800 hover:cursor-pointer"
          />
          <Button
            type={""}
            onClick={() => {
              props.onCancelClick();
            }}
            name="Cancel"
            className="text-white bg-red-700 hover:bg-red-800 hover:cursor-pointer"
          />
        </div>
      </div>
    </div>
  );
};

export default EditGigForm;
