import React, { useState } from "react";
import TextInput from "./TextInput";
import TextArea from "../Basic/TextArea";
import Button from "./Button";
import AdminRequests from "../../utils/adminRequests";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

let GigForm = (props) => {
  // VARIABLES
  //   let navigate = useNavigate();
  let admin = useSelector((state) => state.adminAuth);
  // USESTATE
  let [brand, setBrand] = useState("");
  let [description, setDescription] = useState("");
  let [reward, setReward] = useState(0);
  let [error, setError] = useState("");

  // This method creates a gig
  let createGig = async () => {
    if (brand === "" || description === "" || reward < 1) {
      setError("Please ensure all fields are populated.");
      return;
    }

    let payload = {
      brand: brand,
      description: description,
      reward: reward,
      accessToken: admin.accessToken,
    };
    await createGigThunk(payload);
  };

  // Request thunks methods
  let createGigThunk = async (payload) => {
    AdminRequests.createGig(payload)
      .then((data) => {
        if (data.error) {
          //   navigate("/error");
        } else {
          props.onConfirmClick();
          // dispatch(setValues(data["data"]));
          // navigate("/");
        }
      })
      .catch(() => {});
  };

  return (
    <div>
      <div>
        {/* <h1 className="text-black text-[36px] font-bold ">Create Gig</h1> */}
        <span className="text-red-700 text-[14px] underline">{error}</span>
      </div>
      {/* <form action=""> */}
      <div>
        <div className="flex flex-wrap gap-[20px] justify-between">
          <div className="w-full lg:w-72">
            <TextInput
              id="brand"
              name="Brand Name"
              placeholder="Brand Name"
              onChange={(e) => {
                setBrand(e.target.value);
                setError(null);
              }}
              type="text"
              classNames="border-2 w-full"
            />
          </div>
          <div className="w-full lg:w-72">
            <TextInput
              id="currencyInput"
              name="Reward"
              placeholder="0.00"
              onChange={(e) => {
                setReward(e.target.value);
                setError(null);
              }}
              type="number"
              classNames="border-2 w-full"
            />
          </div>
        </div>
        <div>
          <TextArea
            id="description"
            name="Description"
            placeholder="Gig description"
            onChange={(e) => {
              setDescription(e.target.value);
              setError(null);
            }}
            classNames="border-2 w-full"
          />
        </div>
        <div className="flex flex-row gap-[20px]">
          <Button
            type={"submit"}
            onClick={() => {
              createGig();
              props.onConfirmClick();
              props.onCancelClick();
            }}
            name="Create"
            className="text-white bg-blue-700 hover:bg-blue-800"
          />
          <Button
            type={""}
            onClick={() => {
              props.onCancelClick();
            }}
            name="Cancel"
            className="text-white bg-red-700 hover:bg-red-800"
          />
        </div>
      </div>
      {/* </form> */}
    </div>
  );
};

export default GigForm;
