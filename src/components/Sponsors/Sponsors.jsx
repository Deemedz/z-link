import React from "react";
import DBJ from "../../assets/DBJ.png";
import DIA from "../../assets/dia.png";
import GODDARD from "../../assets/Goddard.png";
import REVUP from "../../assets/Revup.png";
import TBR from "../../assets/tbr.png";

const Sponsors = () => {
  return (
    <div name="sponsor" className="w-full md:h-screen pb-48 sm:pb-10">
      <div className="max-w-[1080px] mx-auto p-4 flex flex-col justify-center w-full h-full">
        <div className="sm:text-left pb-8 pl-4">
          <p className="xl:text-6xl md:text-4xl text-3xl font-bold py-4 px-6">
            Sponsors
          </p>
          <p className="font-medium xl:text-xl md:text-lg text-sm p-4">
            These are the companies that have come together with Z-Rise to bring
            you this product.
          </p>
        </div>

        <div className="grid grid-cols-6 gap-4 ">{/*grid sm:grid-cols-3 md:grid-cols-5 gap-4*/}
          <a
            href="https://dbankjm.com/ignite/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <div
              style={{ backgroundImage: `url(${DBJ})` }}
              className="w-[96px] h-[96px] md:w-[200px] md:h-[200px] lg:w-[250px] lg:h-[250px] hover:scale-125 duration-300 bg-no-repeat bg-center bg-contain shadow-lg shadow-[#040c16] group container  rounded-md flex justify-center items-center mx-auto content-div"
            >
              <div className="opacity-100 group-hover:opacity-0 duration-200"></div>
            </div>
          </a>

          <a
            href="https://www.trustfortheamericas.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <div
              style={{ backgroundImage: `url(${DIA})` }}
              className="w-[96px] h-[96px] md:w-[200px] md:h-[200px] lg:w-[250px] lg:h-[250px] hover:scale-125 duration-300 bg-no-repeat bg-center bg-contain shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div"
            >
              <div className="opacity-0 group-hover:opacity-100 duration-200"></div>
            </div>
          </a>

          <a
            href="https://www.goddardenterprisesltd.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <div
              style={{ backgroundImage: `url(${GODDARD})` }}
              className="w-[96px] h-[96px] md:w-[200px] md:h-[200px] lg:w-[250px] lg:h-[250px] hover:scale-125 duration-300 bg-no-repeat bg-center bg-contain shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div"
            >
              <div className="opacity-0 group-hover:opacity-100 duration-200"></div>
            </div>
          </a>

          <a
            href="https://www.revupcaribbean.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <div
              style={{ backgroundImage: `url(${REVUP})` }}
              className="w-[96px] h-[96px] md:w-[200px] md:h-[200px] lg:w-[250px] lg:h-[250px] hover:scale-125 duration-300 bg-no-repeat bg-center bg-contain shadow-lg shadow-[#040c16] group container  rounded-md flex justify-center items-center mx-auto content-div"
            >
              <div className="opacity-0 group-hover:opacity-100 duration-200"></div>
            </div>
          </a>

          <a
            href="https://www.techbeach.net/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <div
              style={{ backgroundImage: `url(${TBR})` }}
              className="w-[96px] h-[96px] md:w-[200px] md:h-[200px] lg:w-[250px] lg:h-[250px] hover:scale-125 duration-300 bg-no-repeat bg-center bg-contain shadow-lg shadow-[#040c16] group container  rounded-md flex justify-center items-center mx-auto content-div"
            >
              <div className="opacity-0 group-hover:opacity-100 duration-200"></div>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
};

export default Sponsors;
