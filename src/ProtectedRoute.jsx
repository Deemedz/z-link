import React from "react";
import { Outlet, Navigate } from "react-router-dom";
import { useSelector } from "react-redux";

let ProtectedRoute = (props) => {
  let user = useSelector((state) => state.auth);
  let admin = useSelector((state) => state.adminAuth);

  return props.isAdmin ? (
    admin.accessToken !== "" ? (
      <Outlet />
    ) : (
      <Navigate to="/login/admin" />
    )
  ) : props.isUser ? (
    user.accessToken !== "" ? (
      <Outlet />
    ) : (
      <Navigate to="/login" />
    )
  ) : null;
};

export default ProtectedRoute;
