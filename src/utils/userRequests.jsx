import request from "./requestWrapper";

function get(id) {
  return request({
    url: `/message/${id}`,
    method: "GET",
  });
}

// This is the register request method
function register({ firstName, lastName, email, password }) {
  return request({
    url: "/user",
    method: "POST",
    data: {
      firstName,
      lastName,
      email,
      password,
    },
  });
}

// This is the login request method
function login({ email, password }) {
  return request({
    url: "/login/user",
    method: "POST",
    data: {
      email,
      password,
    },
  });
}

// This is the update request
function update({
  userId,
  firstName,
  lastName,
  email,
  phone,
  bio,
  accessToken,
}) {
  let jsonBody = {
    firstName: firstName,
    lastName: lastName,
    email: email,
    phoneNumber: phone,
    bio: bio,
  };

  return request({
    url: `/user/${userId}`,
    method: "PATCH",
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
    data: jsonBody,
  });
}

// function getUser(userId, accessToken) {
function getUser(payload) {
  let newId = payload.userId;
  return request({
    url: `/user/${payload.userId}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${payload.accessToken}`,
    },
    data: {
      newId,
    },
  });
}

// this function gets all the active gigs for the user 
function getActiveGigs(payload){
  return request({
    url:"/jobs/active",
    method: "GET",
    headers: {
      Authorization: `Bearer ${payload.accessToken}`,
    },
  })
}
// TODO!! To be continued 
// function updatePassword(payload){
//   url:
//   method:
//   headers:{}
//   data:
// }

const UserRequests = {
  register,
  login,
  update,
  getUser,
  getActiveGigs
};

export default UserRequests;
