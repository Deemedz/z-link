import request from "./requestWrapper";
// admin login request
function login({ id, password }) {
  return request({
    url: "/login/admin",
    method: "POST",
    data: {
      id,
      password,
    },
  });
}
// gets all the users to show admin
function getAllUsers({ accessToken }) {
  return request({
    url: "/users",
    method: "GET",
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}
// gets all the gigs
function getAllGigs({ accessToken }) {
  return request({
    url: "/jobs",
    method: "GET",
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}
// creates a new gig
function createGig({ brand, description, reward, accessToken }) {
  return request({
    url: "/job",
    method: "POST",
    data: {
      brand,
      description,
      reward,
    },
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}
// updates gig
function updateGig({ gig_id, brand, description, reward, accessToken }) {
  return request({
    url: `/job/${gig_id}`,
    method: "PATCH",
    data: {
      brand,
      description,
      reward,
    },
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}

// updates gig
function updateStatus({ gig_id, status, accessToken }) {
  return request({
    url: `/job//status/${gig_id}`,
    method: "PATCH",
    data: {
      status
    },
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}

let AdminRequests = { login, getAllUsers, getAllGigs, createGig, updateGig, updateStatus };

export default AdminRequests;
