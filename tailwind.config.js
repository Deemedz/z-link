/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ["./src/**/*.{js,jsx,ts,tsx}"],
	theme: {
		container: {
			center: true,
			padding: "2rem",
		},
		extend: {
			animation: {
				rocketBounce: "rocketBounce 3s ease-in-out infinite",
			},
			keyframes: {
				rocketBounce: {
					"0%, 100%": { transform: "translateY(-25%)" },
					"50%": { transform: "translateY(-15%)" },
				},
			},
		},
		screens: {
			'sm': '360px',
			// => @media (min-width: 360px) { ... }

			'md': '640px',
			// => @media (min-width: 640px) { ... }

			'lg': '810px',
			// => @media (min-width: 810px) { ... }

			'xl': '1024px',
			// => @media (min-width: 1024px) { ... }
		}
	},
	plugins: [
		require('tailwind-scrollbar'),
	],
};
